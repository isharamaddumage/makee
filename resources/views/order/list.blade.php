@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h2 class="label-info"></h2>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Orders
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">

                <p>
                    <div class="alert" id="flash_message" style="display:none">
                    </div>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="order_table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cust name</th>
                                <th>Delivery Address</th>
                                <th>Total Price</th>
                                <th>Status</th>

                                <th></th>
                            </tr>
                        </thead>
                    </table>
                    @include('order.view')
                </p>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script type="text/javascript" src="{{ URL::asset('/js/app/order/app.order.list.js') }}"></script>
@endsection