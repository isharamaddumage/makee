<div class="modal fade" id="activateModal" tabindex="-1" role="dialog" aria-labelledby="activateModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="activateModalLabel">Activate User</h5>
            </div>
            <div class="modal-body">
                <form role="form">
                 <input type="hidden" id="uid">
                   <div class="form-group">
                        <label>User Level</label>
                        <select class="form-control" id="user_level">
                            @if(count(Config::get('config.userLevel')))>0)
                            @foreach(Config::get('config.userLevel') as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                            @else
                            <option value="1">--</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Activate</label>
                        <select class="form-control" id="activate">
                            @if(count(Config::get('config.activate')))>0)
                            @foreach(Config::get('config.activate') as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                            @else
                            <option value="1">--</option>
                            @endif
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default btn-primary" id="activate_submit" style="float: right;">Save </button>
                    <div style="clear: both;"></div>
                </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>