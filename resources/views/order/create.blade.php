@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h2 class="label-info"></h2>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Place order
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <p>
                    <form role="form" action="{!! url('/order/place'); !!}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td><label>Delivery Address</label></td>
                                    <td colspan="3"><textarea class="form-control" rows="2" id="delivery_address" name="delivery_address" ></textarea></td>
                                </tr>
                                <tr>
                                    <td><label>Item 1</label></td>
                                    <td><select class="form-control" id="item[]" name ="item[]">
                                        @if(count(Config::get('items.items')))>0)
                                        @foreach(Config::get('items.items') as $key=>$item)
                                        <option value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                        @else
                                        <option value="1">--</option>
                                        @endif
                                    </select></td>
                                    <td><label>Qty</label></td>
                                    <td><input class="form-control"  id ="qty[]" name ="qty[]" value=""></td>
                                </tr>
                                <tr>
                                    <td><label>Item 2</label></td>
                                    <td><select class="form-control" id="item[]" name ="item[]">
                                        @if(count(Config::get('items.items')))>0)
                                        @foreach(Config::get('items.items') as $key=>$item)
                                        <option value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                        @else
                                        <option value="1">--</option>
                                        @endif
                                    </select></td>
                                    <td><label>Qty</label></td>
                                    <td><input class="form-control"  id ="qty[]" name ="qty[]" value=""></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-default btn-primary" id="profile_submit" style="float: right;">Save </button>
                        <div style="clear: both;"></div>
                    </form>
                </p>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script type="text/javascript" src="{{ URL::asset('/js/app/user/app.user.profile.js') }}"></script>
@endsection