<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'order'], function () {
	Route::get('/place', [
		'uses' => 'OrderController@create',
	]);
	Route::post('/place', [
		'uses' => 'OrderController@save',
	]);
	Route::get('/list', [
		'uses' => 'OrderController@viewList',
	]);
	Route::get('/list/data', [
		'uses' => 'OrderController@getList',
	]);
});

Route::get('/home', 'HomeController@index');

Route::get('/clear-cache', function () {
	$exitCode = Artisan::call('cache:clear');
	// return what you want
});
