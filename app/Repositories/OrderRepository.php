<?php

namespace App\Repositories;

use App\Contracts\OrderInterface;
use App\Order;
use App\OrderItem;
use App\Product;
use Datatables;

class OrderRepository implements OrderInterface {

	protected $order;

	/**
	 * Create a new repository instance.
	 *
	 * @return void
	 */
	public function __construct(Order $order) {
		$this->order = $order;
	}

	/**
	 * Get orders datatable json
	 * @return JSON Datatable json object
	 */
	function data() {
		$orders = Order::leftjoin(
			'users',
			'orders.user_id',
			'=',
			'users.id'
		)->select(array(
			'orders.id AS id',
			'orders.delivery_address AS delivery_address',
			'orders.total_price AS total_price',
			'orders.status AS status',
			'users.name AS name',

		));

		return Datatables::of($orders)
			->make(true);

	}

	function save(Array $payload) {
		$order = $this->order;

		try {

			\DB::beginTransaction();
			$order->user_id = \Auth::user()->id;
			$order->delivery_address = $payload['delivery_address'];
			$order->total_price = 0;
			$order->status = "1";
			$order->save();

			$total_price = 0;
			for ($i = 0; $i < count($payload['item']); $i++) {
				try {
					\DB::beginTransaction();
					$item = new OrderItem;
					$item->order_id = $order->id;
					$item->product_id = $payload['item'][$i];
					$item->qty = $payload['qty'][$i];
					$product = Product::where('id', $payload['item'][$i])->first();
					$total_price = $total_price + ($payload['qty'][$i] * $product->unit_price);
					$item->save();
					\DB::commit();

				} catch (\Exception $e) {
					\DB::rollBack();
				}

			}
			$order->total_price = $total_price;
			$order->save();
			\DB::commit();

			return $order;
		} catch (\Exception $e) {
			\DB::rollBack();
			return false;
		}

	}

}