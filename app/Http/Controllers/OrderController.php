<?php

namespace App\Http\Controllers;

use App\Contracts\OrderInterface;
use Illuminate\Http\Request;
use Validator;

class OrderController extends Controller {

	protected $order;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(OrderInterface $order) {
		$this->middleware('auth');
		$this->order = $order;
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('home');
	}

	public function create() {
		return view('order.create');
	}

	public function save(Request $request) {
		$rules = [
			'delivery_address' => ['required'],

		];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			return redirect('/order/place');
		} else if ($validator->passes()) {
			return $this->order->save($request->all());
		}

	}

	public function viewList() {
		return view('order.list');
	}

	public function getList() {
		return $this->order->data();
	}
}
