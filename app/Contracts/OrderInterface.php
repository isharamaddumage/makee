<?php

namespace App\Contracts;

interface OrderInterface {

	/**
	 * Get datatable json
	 * @return JSON Datatable json object
	 */
	public function data();

	public function save(Array $payload);

}
