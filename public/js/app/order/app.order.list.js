$(document).ready(function() {

    $("#flash_message").css("display", "none");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var order_table = $('#order_table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        "paging": true,
        "lengthMenu": [10,20,50, 100],
        pagingType: "simple_numbers",
        ajax: url + "/order/list/data",
        columns: [{
            data: 'id',
            width: "10%",
            name: 'id',
            "searchable": true,
            "orderable": false,
        },{
            data: 'name',
            width: "20%",
            name: 'users.name',
            "searchable": true,
            "orderable": false,
        },{
            data: 'delivery_address',
            width: "20%",
            name: 'orders.delivery_address ',
            "searchable": true,
            "orderable": false,
        },{
            data: 'total_price',
            width: "20%",
            name: 'orders.total_price',
            "searchable": true,
            "orderable": false,
        }, {
            data: null,
            name: 'orders.status',
            "searchable": false,
            render: function(data, type, row) {
                var status = "";
                if (data.status == 1) {
                    status = "Pending"
                }
                if (data.status == 2) {
                    status = "Processing"
                }
                if (data.status == 3) {
                    status = "Delivered"
                }
                
                return status;
            },

        },{
            data: null,
            className: "center",
            "searchable": false,
            "orderable": false,
            render: function(data, type, row) {
                return '<a href="" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>' +
                    '<a href="" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>' +
                    '<i class="glyphicon glyphicon-trash btn btn-danger btn-xs"></i>';
            },
            //defaultContent: '<button type="button" class="btn btn-warning btn-view-pdf" data-toggle="modal" id='+data.firstname+' data-target="#myModal">Edit</button>'
        }]
    });

   

    

  

    

});